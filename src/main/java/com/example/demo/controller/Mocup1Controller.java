package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@RequestMapping("new/*")
@Controller
public class Mocup1Controller {

	@GetMapping()
	public String open() {
		return "Mocup1";
	}
}


