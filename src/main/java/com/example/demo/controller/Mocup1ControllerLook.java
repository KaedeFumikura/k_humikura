package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.bean.Mocup1FormBean;

@Controller
@RequestMapping("/MocupLooking/*")
public class Mocup1ControllerLook {

	@PostMapping()
	public String open(@ModelAttribute Mocup1FormBean mocup1FormBean, Model model) {
		model.addAttribute("mocup1FormBean" , mocup1FormBean);
		return "MocupLook1";
	}

}
