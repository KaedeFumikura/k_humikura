package com.example.demo.model.bean;

public class Mocup1FormBean {
	//メールアドレス
	private String mail;
	//パスワード
	private String passWord;
	//名前
	private String name;

	//Mailのgettersetter
	public String getMail(){
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}

	//passWordのgettersetter
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	//名前のgettersetter
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}



}
